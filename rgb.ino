#define LEDR 9
#define LEDG 10
#define LEDB 11

void setup() {
  Serial.begin(9600);
  pinMode(LEDR, OUTPUT);
  pinMode(LEDG, OUTPUT);
  pinMode(LEDB, OUTPUT);
}

void turnOffAll() {
  digitalWrite(LEDR, LOW);
  digitalWrite(LEDG, LOW);
  digitalWrite(LEDB, LOW);
}

void loop() {
  if (Serial.available() > 0) {
    String input = Serial.readStringUntil('\n');  // Read the input as a string until newline character

    turnOffAll();  // Ensure all LEDs are off before turning any on

    if (input == "r") {
      digitalWrite(LEDR, HIGH);
    } else if (input == "g") {
      digitalWrite(LEDG, HIGH);
    } else if (input == "b") {
      digitalWrite(LEDB, HIGH);
    } else if (input == "on") {
      digitalWrite(LEDR, HIGH);
      digitalWrite(LEDG, HIGH);
      digitalWrite(LEDB, HIGH);
    }
  }
  delay(100);
}
